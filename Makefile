.DEFAULT_GOAL := help

ifdef DEBUG 
DRY_RUN := echo
else
DRY_RUN :=
endif

INVENTORY ?= localhost, -c local
PARAMS_ADD ?=

PLAYBOOKS :=
PLAYBOOKS += dind
PLAYBOOKS += gitlab-runner
PLAYBOOKS += gitlab-runner-install-docker
PLAYBOOKS += verify-prerequisities

PLAYBOOKS_VERIFY :=
PLAYBOOKS_VERIFY += dind-verify
PLAYBOOKS_VERIFY += gitlab-runner-verify

$(PLAYBOOKS): verify-prerequisities requirements
	@$(DRY_RUN) ansible-playbook -i $(INVENTORY) $(PARAMS_ADD) -vv $@.yml
.PHONY: $(PLAYBOOKS)

$(PLAYBOOKS_VERIFY): verify-prerequisities requirements
	@$(DRY_RUN) ansible-playbook -i $(INVENTORY) $(PARAMS_ADD) -vv $@.yml
.PHONY: $(PLAYBOOKS_VERIFY)

help:
	@echo
	@echo Targets:
	@cat Makefile* | grep '^.PHONY: .* #' | sort | sed 's/\.PHONY: \(.*\) # \(.*\)/\1: \2/'
	@echo
	@echo Variables that makes sense:
	@echo
	@echo " DEBUG: only (now: $(DRY_RUN)): show commands to be run"
	@echo " INVENTORY: inventory file to use"
	@echo " PARAMS_ADD: additional command line params for ansible-playbook"
	@echo
.PHONY: help  # show help


gitlab-runner-all: | gitlab-runner gitlab-runner-verify gitlab-runner-install-docker
	@echo Success: $@
.PHONY: gitlab-runner-all  # | gitlab-runner gitlab-runner-verify gitlab-runner-install-docker

docker-in-podman: | dind dind-verify
	@echo Success: $@
.PHONY: docker-in-podman # | dind dind-verify

package-versions: verify-podman verify-containernetworking-plugins


COLLECTION_PATH ?= $(HOME)/.ansible/collections
COLLECTION_REQUIREMENTS := $(abspath requirements.yml)
$(COLLECTION_REQUIREMENTS):
	# $@: installing collection from (should it exist)
	@if [ -r $@ ]; then \
		$(DRY_RUN) ansible-galaxy collection install \
		--requirements-file $@ \
		--ignore-certs \
		--collections-path $(COLLECTION_PATH) \
		; \
	fi
	@echo "Success $@."
.PHONY: $(COLLECTION_REQUIREMENTS)

requirements: $(COLLECTION_REQUIREMENTS) 
	@echo "Success $@."
.PHONY: requirements
