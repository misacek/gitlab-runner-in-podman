
# Proof of concept of gitlab-runner docker executor with podman only.

Problem: RHEL/Centos 8 have no docker. gitlab-runner cannot use natively podman. 
Idea: One pod runs dind container the other runs gitlab-runner container. gitlab-runner connects to dind container. Profit.

## Prerequisities

The following packages needs to be installed:

	Ansible >= 2.9.13 (epel-8)
	podman >= 1.6.4
	containernetworking-plugins >= 0.8.7 (available in [fedora33][fedora33 mirrors])

Other than Ansible packages can be install using `make verify-prerequisities` target.

## DIND

Docker image [stable-dind][stable-dind] image runs docker and expose it through port 2376.

    * ports 
      * 2376
    * volumes
      * `/certs`
      * `/etc/docker`

The connection needs to be done using ssl certificates under `/certs/client` directory (`ca.pem`, `cert.pem`, `key.pem`). 

To use dind container instead of local socket you need to do the following:
    * export `DOCKER_HOST=<dind ip>:2376` to env causing docker to connect to remote host instead of local socket
    * export `DOCKER_TLS_VERIFY=1` to env causing docker client to use cert authentication
    * allow docker to find your client certificates (choose one) 
        * place them under in `~/.docker/` 
        * export their directory to env `DOCKER_CERT_PATH`.

Client connection to remote docker host can be checked [using curl][docker-tls-client-modes]

## Example workflow against two freshly installed virts.

    localhost$ export DIND_HOST=virt-001
    localhost$ export RUNNER_HOST=virt-002

    localhost$ make docker-in-podman INVENTORY=${DIND_HOST},

    localhost$ export INVENTORY=${RUNNER_HOST},
    localhost$ export PARAMS_ADD="-e dind_vip=${DIND_HOST} -e dind_vip_port=38133"
    localhost$ make gitlab-runner-all

## Problems to be solved
    - dind seems to need to run as root (needed?)
        https://docs.docker.com/engine/security/rootless/#prerequisites
    - gitlab-runner should run completely rootless

    - [bz1902731][bz1902731]: publishing specific port will not publish it where publish-al will


[stable-dind]: https://hub.docker.com/layers/docker/library/docker/stable-dind/images/sha256-331dafcf4b675f6ace9212a113d973a9cf56ea55c33415e1853e6ea1b474e0b6?context=explore
[docker-tls-client-modes]: https://docs.docker.com/engine/security/https/#client-modes
[fedora33 mirrors]: https://mirrors.fedoraproject.org/mirrorlist?repo=fedora-33&arch=x86_64
[bz1902731]: https://bugzilla.redhat.com/show_bug.cgi?id=1902731
